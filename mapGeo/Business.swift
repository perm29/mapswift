//
//  Business.swift
//  mapGeo
//
//  Created by ii on 15.05.23.
//
import MapKit

struct Business: Identifiable{
    let id = UUID()
    let name: String
    let placemark: MKPlacemark
    let coordinate: CLLocationCoordinate2D
    let phoneNumber: String
    let website: URL?
}
