//
//  mapGeoApp.swift
//  mapGeo
//
//  Created by ii on 15.05.23.
//

import SwiftUI

@main
struct mapGeoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
