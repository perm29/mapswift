//
//  BusinessView.swift
//  mapGeo
//
//  Created by ii on 15.05.23.
//

import SwiftUI

struct BusinessView: View {
    @ObservedObject var mapController: MapController
    
    var body: some View {
        VStack{
            HStack(alignment: .top){
                VStack(alignment: .leading){
                    Text(mapController.selectBusinessName)
                        .font(.title)
                    
                    Text(mapController.selectBusinessPlacemark)
                }
                Spacer()
                
                Button{
                    mapController.isBusinessViewShowing.toggle()
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(.secondary)// press shift command l to open lib for choose some icons etc
                }
                
               
            }
            HStack{
                ForEach(mapController.actions){
                    action in
                    Button{
                        action.handler()
                    } label: {
                        VStack{
                            Image(systemName: action.image)
                            Text(action.title)
                        }
                        .frame(maxWidth: .infinity)
                    }
                    .buttonStyle(.bordered)
                }
            }
    
        }
    }
}

struct BusinessView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessView(mapController: MapController())
            .previewLayout(.sizeThatFits) // so we can see only view that we are making
    }
}
