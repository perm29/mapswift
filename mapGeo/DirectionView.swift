//
//  DirectionView.swift
//  mapGeo
//
//  Created by ii on 18.05.23.
//
import SwiftUI
import MapKit

struct DirectionView: View {
    @State private var sourceCoordinate = CoordinateWrapper(latitude: 37.755221, longitude: -122.452762)
    @State private var destinationCoordinate = CoordinateWrapper(latitude: 37.771516, longitude: -122.468647)
    @State private var route: MKRoute?
    
    var body: some View {
        VStack {
            Map(coordinateRegion: .constant(region), annotationItems: [sourceCoordinate, destinationCoordinate]) { coordinate in
                MapMarker(coordinate: coordinate.coordinate)
            }
            
            Button("Calculate Route") {
                calculateRoute()
            }
            
            if let route = route {
                Text("Distance: \(route.distance / 1000) km")
                Text("Expected Travel Time: \(route.expectedTravelTime / 60) mins")
            }
        }
    }
    
    private func calculateRoute() {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: sourceCoordinate.coordinate))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate.coordinate))
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate { response, error in
            guard let route = response?.routes.first else {
                return
            }
            
            self.route = route
        }
    }
    
    private var region: MKCoordinateRegion {
        MKCoordinateRegion(center: sourceCoordinate.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
    }
}

struct CoordinateWrapper: Identifiable {
    let id = UUID()
    let coordinate: CLLocationCoordinate2D
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

struct DirectionView_Previews: PreviewProvider {
    static var previews: some View {
        DirectionView()
    }
}
