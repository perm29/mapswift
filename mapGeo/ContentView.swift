//
//  ContentView.swift
//  mapGeo
//
//  Created by ii on 15.05.23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var mapController = MapController()
    var body: some View {
        NavigationStack{
            MapView(mapController: mapController)
        }
        .searchable(text: $mapController.searchTerm)
        .onSubmit(of: .search) {
            mapController.search()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


