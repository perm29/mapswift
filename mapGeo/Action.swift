//
//  Action.swift
//  mapGeo
//
//  Created by ii on 15.05.23.
//

import Foundation

struct Action: Identifiable{
    let id = UUID()
    let title: String
    let image: String
    let handler: () -> Void
}
